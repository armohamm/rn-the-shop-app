import React from 'react';
import { Button, FlatList, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import CartItem from '../../components/shop/CartItem';
import Colors from '../../constants/Colors';
import * as cartAction from '../../store/actions/cart';
import * as ordersAction from '../../store/actions/orders';

const CartScreen = props => {
    const cartTotalAmount = useSelector(state => state.cart.totalAmount);
    const cartItems = useSelector(state => {
        const transformedCartItems = [];
        for (const key in state.cart.items) {
            transformedCartItems.push({
                productId: key,
                productTitle: state.cart.items[key].productTitle,
                productPrice: state.cart.items[key].productPrice,
                quantity: state.cart.items[key].quantity,
                sum: state.cart.items[key].sum,
            })
        }

        return transformedCartItems.sort((a,b) =>
            a.productId > b.productId ? 1 : -1
        );
    })

    const dispatch = useDispatch();

    return (
        <View style={styles.screen}>
            <View style={styles.summary}>
                <Text style={styles.summaryText}>Total: <Text style={styles.amount}>${cartTotalAmount.toFixed(2)}</Text></Text>
                <Button
                    color={Colors.accent}
                    title="Order now"
                    disabled={cartItems.length === 0}
                    onPress={() => {
                        dispatch(ordersAction.addOrder(cartItems, cartTotalAmount))
                    }}
                />
            </View>
            <FlatList data={cartItems} keyExtractor={item => item.productId} renderItem={itemData => (
                <CartItem
                    title={itemData.item.productTitle}
                    quantity={itemData.item.quantity}
                    amount={itemData.item.sum}
                    deletable
                    onRemove={() => {
                        dispatch(cartAction.removeFromCart(itemData.item.productId))
                    }}
                />
            )}/>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        margin: 20
    },
    summary: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'white',
        borderRadius: 10,
        marginBottom: 20,
        padding: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
    },
    summaryText: {
        fontFamily: 'open-sans-bold',
        fontSize: 18
    },
    amount: {
        color: Colors.primary
    }
});

CartScreen.navigationOptions = {

}

export default CartScreen;