import React from 'react';
import { Alert, Button, FlatList, StyleSheet } from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import { useDispatch, useSelector } from 'react-redux';
import ProductItem from '../../components/shop/ProductItem';
import HeaderButton from '../../components/ui/HeaderButton';
import Colors from '../../constants/Colors';
import * as productsAction from '../../store/actions/products';

const UserProductsScreen = props => {
    const products = useSelector(state => state.products.userProducts);

    const dispatch = useDispatch();

    const handleSelectedItem = (id) => {
        props.navigation.navigate('EditProduct', { pid: id })
    }

    const deleteHandler = (id) => {
        Alert.alert('Are you sure?', 'Do you really want to delete this item?', [
            {text: 'No', style: 'default'},
            {
                text: 'Yes',
                style: 'destructive',
                onPress: () => {
                    dispatch(productsAction.deleteProduct(id));
                }
            }
        ])
    }

    return (
        <FlatList data={products} renderItem={itemData =>
            <ProductItem
                image={itemData.item.imageUrl}
                title={itemData.item.title}
                price={itemData.item.price}
                onSelect={() => {
                    handleSelectedItem(itemData.item.id);
                }}
            >
                <Button
                    color={Colors.primary}
                    title="Edit Product"
                    onPress={() => {
                        handleSelectedItem(itemData.item.id);
                    }}
                />
                <Button
                    color='red'
                    title="Delete"
                    onPress={deleteHandler.bind(this, itemData.item.id)}
                />
            </ProductItem>
        } />
    );
};

const styles = StyleSheet.create({

});

UserProductsScreen.navigationOptions = navData => {
    return {
        headerLeft: () => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Menu"
                    iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
                    onPress={() => {
                        navData.navigation.toggleDrawer();
                    }}
                />
            </HeaderButtons>
        ),
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title="Add"
                    iconName={Platform.OS === 'android' ? 'md-add-circle' : 'ios-add-circle'}
                    onPress={() => {
                        navData.navigation.navigate('EditProduct');
                    }}
                />
            </HeaderButtons>
        ),
    }
}

export default UserProductsScreen;