import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { Platform, Button, View, SafeAreaView } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { useDispatch } from 'react-redux';

import CartScreen from '../screens/shop/CartScreen';
import OrdersScreen from '../screens/shop/OrdersScreen';
import ProductDetailScreen from '../screens/shop/ProductDetailScreen';
import ProductsOverviewScreen from '../screens/shop/ProductsOverviewScreen';
import StartupScreen from '../screens/StartupScreen';
import AuthScreen from '../screens/user/AuthScreen';
import EditProductScreen from '../screens/user/EditProductScreen';
import UserProductsScreen from '../screens/user/UserProductsScreen';

import Colors from '../constants/Colors';

import * as authActions from '../store/actions/auth';

const defaultNavOptions = {
    headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
    },
    headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary
}

const ProductsNavigator = createStackNavigator(
    {
        ProductsOverview: {
            screen: ProductsOverviewScreen,
            navigationOptions: {
                headerTitle: 'All Products'
            }
        },
        ProductDetail: {
            screen: ProductDetailScreen,
        },
        Cart: {
            screen: CartScreen,
            navigationOptions: {
                headerTitle: 'Your Cart'
            }
        }
    }, {
        navigationOptions: {
            drawerLabel: 'Products ',
            drawerIcon: drawerConfig => (
                <Ionicons
                    name={Platform.OS === 'android' ? 'md-cart' : 'ios-cart'}
                    size={23}
                    color={drawerConfig.tintColor}
                />
            )
        },
        defaultNavigationOptions: defaultNavOptions
    }
);

const OrdersNavigator = createStackNavigator(
    {
        Orders: {
            screen: OrdersScreen,
            navigationOptions: {
                headerTitle: 'Your Orders'
            },
        }
    }, {
        navigationOptions: {
            drawerLabel: 'Orders ',
            drawerIcon: drawerConfig => (
                <Ionicons
                    name={Platform.OS === 'android' ? 'md-list' : 'ios-list'}
                    size={23}
                    color={drawerConfig.tintColor}
                />
            )
        },
        defaultNavigationOptions: defaultNavOptions
    }
);

const UserProductsNavigator = createStackNavigator(
    {
        UserProducts: {
            screen: UserProductsScreen,
            navigationOptions: {
                headerTitle: 'Your Products'
            }
        },
        EditProduct: {
            screen: EditProductScreen
        }
    }, {
        navigationOptions: {
            drawerLabel: 'My Products ',
            drawerIcon: drawerConfig => (
                <Ionicons
                    name={Platform.OS === 'android' ? 'md-create' : 'ios-create'}
                    size={23}
                    color={drawerConfig.tintColor}
                />
            )
        },
        defaultNavigationOptions: defaultNavOptions
    }
);

const AuthNavigator = createStackNavigator({
    Auth: AuthScreen
}, {
    defaultNavigationOptions: defaultNavOptions
})

const ShopNavigator = createDrawerNavigator(
    {
        ProductsNav: {
            screen: ProductsNavigator,
            navigationOptions: {
                headerTitle: 'All Products'
            }
        },
        OrdersNav: {
            screen: OrdersNavigator,
            navigationOptions: {
                headerTitle: 'Your Orders',
            }
        },
        UserProductsNav: {
            screen: UserProductsNavigator,
            navigationOptions: {
                headerTitle: 'Your Products',
            }
        }
    }, {
        contentOptions: {
            activeTintColor: Colors.primary
        },
        contentComponent: props => {
            const dispatch = useDispatch();

            return (
                <View style={{ flex: 1, paddingTop: 20 }}>
                    <SafeAreaView>
                        <DrawerItems {...props} />
                        <Button title="Logout" color={Colors.primary} onPress={() => {
                            dispatch(authActions.logout());
                            // props.navigation.navigate('Auth');
                        }} />
                    </SafeAreaView>
                </View>
            )
        }
    }
)

const MainNavigator = createSwitchNavigator({
    StartupScreen: StartupScreen,
    Auth: AuthNavigator,
    Shop: ShopNavigator
})

export default createAppContainer(MainNavigator);