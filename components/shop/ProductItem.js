import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const ProductItem = props => {
    return (
        <TouchableOpacity style={styles.container} onPress={props.onSelect}>
            <View>
                <Image source={{ uri: props.image }} style={styles.image} />
                <View style={styles.details}>
                    <Text style={styles.title}>{props.title}</Text>
                    <Text style={styles.price}>${props.price.toFixed(2)}</Text>
                </View>
                <View style={styles.buttonContainer}>
                    {props.children}
                </View>
            </View>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: 300,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#CCCCCC',
        margin: 20,
        overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
    },
    image: {
        width: '100%',
        height: '60%',
        borderRadius: 5
    },
    details: {
        height: '15%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontFamily: 'open-sans-bold',
        fontSize: 18,
        marginVertical: 5
    },
    price: {
        fontFamily: 'open-sans',
        fontSize: 14,
        color: '#888888'
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '25%',
        marginHorizontal: 20
    }
});

export default ProductItem;