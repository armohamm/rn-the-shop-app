import React, { useState } from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';
import Colors from '../../constants/Colors';
import CartItem from './CartItem';

const OrderItem = props => {
    const [ showDetails, setShowDetails ] = useState(false);

    return (
        <View style={styles.orderItem}>
            <View style={styles.summary}>
                <Text style={styles.totalAmount}>${props.amount.toFixed(2)}</Text>
                <Text style={styles.date}>{props.date}</Text>
            </View>
            {showDetails && (
                <View>
                    {props.items.map(cartItem =>
                        <CartItem
                            key={cartItem.productId}
                            quantity={cartItem.quantity}
                            amount={cartItem.sum}
                            title={cartItem.productTitle}
                            deletable={false}
                        />
                    )}
                </View>
            )}
            <Button style={styles.button} color={Colors.primary} title={showDetails ? 'Hide Details' : 'Show Details'} onPress={() => {
                setShowDetails(prevState => !prevState)
            }}/>
        </View>
    );
};

const styles = StyleSheet.create({
    orderItem: {
        backgroundColor: 'white',
        borderRadius: 10,
        marginHorizontal: 20,
        marginVertical: 10,
        padding: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
    },
    summary: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        marginBottom: 15
    },
    totalAmount: {
        fontFamily: 'open-sans-bold',
        fontSize: 16
    },
    date: {
        fontFamily: 'open-sans',
        fontSize: 16,
        color: '#888'
    },
    button: {
        width: '50%'
    }
})

export default OrderItem;