import moment from 'moment';
//import 'moment/min/locales';

class Order {
    constructor(id, items, totalAmount, date) {
        this.id = id;
        this.items = items;
        this.totalAmount = totalAmount;
        this.date = date;
    }

    get readableDate() {
        // return this.date.toLocaleDateString('pt-BR', {
        //     year: 'numeric',
        //     month: 'numeric',
        //     day: 'numeric',
        //     hour: '2-digit',
        //     minute: '2-digit'
        // })
        //moment.locale('pt-br');
        return moment(this.date).format('DD MMMM YYYY (dddd), HH:mm');
    }
};

export default Order;